const striptags = require("striptags");

module.exports = (eleventyConfig) => {
  eleventyConfig.addTransform("wiki-links", function (content, outputPath) {
    if (outputPath && outputPath.endsWith(".html")) {
      // We remove outer brackets from links
      let output = content.replace(/(\[+(\<a(.*?)\<\/a\>)\]+)/g, "$2");
      return output;
    }
    return content;
  });

  let markdownIt = require("markdown-it");
  let markdownItReplaceLink = require("markdown-it-replace-link");
  let taskLists = require("markdown-it-task-lists");
  const wikilinks = require('markdown-it-wikilinks')({uriSuffix: '', relativeBaseURL: '/'})
  let markdownItOptions = {
    html: true,
    linkify: true,
    typographer: true,
    replaceLink: function (link, env) {
      const isRelativePattern = /^(?!http|\/).*/;
      const lastSegmentPattern = /[^\/]+(?=\/$|$)/i;
      const isRelative = isRelativePattern.test(link);

      if (isRelative) {
        const hasLastSegment = lastSegmentPattern.exec(env.page.url);
        // If it's nested, replace the last segment
        if (hasLastSegment && env.page.url) {
          return env.page.url.replace(lastSegmentPattern, link);
        }
        // If it's at root, just add the beginning slash
        return env.page.url + link;
      }

      return link;
    },
  };

  let markdownLib = markdownIt(markdownItOptions)
    .use(markdownItReplaceLink)
    .use(taskLists)
    .use(require("markdown-it-anchor"), {
      permalink: true,
      permalinkBefore: true,
      permalinkSymbol: "§",
      linkClass: "anchor",
    }).use(wikilinks)
    .use(require("markdown-it-emoji"), {})
    .use(require("markdown-it-footnote"));
  eleventyConfig.setLibrary("md", markdownLib);
  eleventyConfig.addCollection("wiki", function (collectionApi) {
    return collectionApi.getAll().sort(function (a, b) {
      return a.url.localeCompare(b.url);
    });
  });
  // based on https://github.com/binyamin/eleventy-garden, MIT license, copyright by @binyamin
  eleventyConfig.addFilter("backlinks", function (pageInfo) {
    const wikilinkRegExp = /\[\[\s?([^\[\]\|\n\r]+)(\|[^\[\]\|\n\r]+)?\s?\]\]/g;

    function caselessCompare(a, b) {
      return a.toLowerCase() === b.toLowerCase();
    }
    const notes = pageInfo.collections.wiki;
    const currentFileSlug = pageInfo.slug; //'personal';

    let backlinks = [];

    // Search the other notes for backlinks
    for (const otherNote of notes) {
      const noteContent = otherNote.template.frontMatter.content;

      // Get all links from otherNote
      const outboundLinks = (noteContent.match(wikilinkRegExp) || []).map(
        (link) =>
          // Extract link location
          link
            .slice(2, -2)
            .split("|")[0]
            .replace(/.(md|markdown)\s?$/i, "")
            .trim()
      );

      // If the other note links here, return related info
      if (
        outboundLinks.some((link) => caselessCompare(link, currentFileSlug))
      ) {
        // Construct preview for hovercards
        let preview = noteContent.slice(0, 240);

        backlinks.push({
          url: otherNote.url,
          title: otherNote.data.title,
          preview,
        });
      }
    }

    return backlinks;
  });
  eleventyConfig.addPassthroughCopy("assets");

  return {
    dir: {
      input: ".",
      output: "_site",
      includes: "_includes",
      layouts: "_layouts",
    },
    templateFormats: [
      //
      "js",
      "md",
      "html",
      "liquid",
      "jpg",
      "njk",
      "icon",
    ],
    passthroughFileCopy: true,
  };
};
